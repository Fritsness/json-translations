import json
import argparse

parser = argparse.ArgumentParser(
    prog="split JSON translations",
    description="Simple python script to split single JSON with multiple translations on multiple JSONs with single translations",
)

parser.add_argument("filename")
parser.add_argument("-k", "--key", default="key")
args = parser.parse_args()

with open(args.filename, "r") as infile:
    filename = infile.name.split(".")[0]
    o = json.load(infile)
    fr = {}
    en = {}
    nl = {}
    for i in o:
        fr[i[args.key]] = i["fr"]
        en[i[args.key]] = i["en"]
        nl[i[args.key]] = i["nl"]
    with open(filename + "_fr.json", "w") as fr_file:
        json.dump(fr, fr_file, indent=2, sort_keys=True, ensure_ascii=False)
    with open(filename + "_en.json", "w") as en_file:
        json.dump(en, en_file, indent=2, sort_keys=True, ensure_ascii=False)
    with open(filename + "_nl.json", "w") as nl_file:
        json.dump(nl, nl_file, indent=2, sort_keys=True, ensure_ascii=False)
